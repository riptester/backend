FROM python:3.9-slim AS main
WORKDIR /
EXPOSE 8080
COPY requirements.txt /
RUN pip install -r /requirements.txt
COPY app /app
ENTRYPOINT [ "python", "-m", "app" ]

FROM main AS test
ENTRYPOINT ["true"]