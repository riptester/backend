import datetime
import re


async def async_iterable(iterable):
    for item in iterable:
        yield item


def parse_timestamp(ts):
    return datetime.datetime.fromisoformat(re.sub(r"(\.\d{3})\d{1,6}(\D|$)", r"\1\2", ts))


def get_runtime(start, end):
    return (parse_timestamp(end) - parse_timestamp(start)).total_seconds()
