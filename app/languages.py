from dataclasses import dataclass
from typing import Callable, Union


@dataclass(frozen=True)
class Language:
    name: str
    image: Union[str, Callable[[str], str]]
    arguments: tuple[str, ...]


LANGUAGES = {
    Language("python3", "python:3", ("python", "/code")),
}

LANGUAGES_MAPPING = {language.name: language for language in LANGUAGES}
