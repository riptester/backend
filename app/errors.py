import abc
import json
from abc import ABC
from enum import IntEnum, unique

from aiohttp import web

from . import config


@unique
class ErrorCode(IntEnum):
    CODE_TOO_LONG = 0
    STDIN_TOO_LONG = 1
    PROTOCOL_ERROR = 2
    ENVIRON_TOO_LONG = 3
    ENVIRON_ENTRY_TOO_LONG = 4
    ENVIRON_ENTRY_MISSING_EQUALS = 5


class HTTPError(web.HTTPError, abc.ABC):
    def __init__(self, /, request: web.Request, **kwargs):
        kwargs.setdefault("text", json.dumps(self.json(request, **kwargs)))
        kwargs.setdefault("content_type", "application/json")
        super().__init__(**kwargs)

    @abc.abstractmethod
    def json(self, /, request: web.Request, **kwargs):
        raise NotImplementedError


class HTTPClientError(HTTPError, ABC):
    pass


class CodeTooLong(HTTPClientError):
    status_code = 413

    def json(self, /, request: web.Request, **kwargs):
        return {
            "message": f"Code is too long (limit is {config.MAX_CODE_LENGTH}",
            "code": ErrorCode.CODE_TOO_LONG,
            "limit": config.MAX_CODE_LENGTH
        }


class StdinTooLong(HTTPClientError):
    status_code = 413

    def json(self, /, request: web.Request, **kwargs):
        return {
            "message": f"Stdin is too long (limit is {config.MAX_STDIN_LENGTH}",
            "code": ErrorCode.STDIN_TOO_LONG,
            "limit": config.MAX_STDIN_LENGTH
        }


class EnvironTooLong(HTTPClientError):
    status_code = 413

    def json(self, /, request: web.Request, **kwargs):
        return {
            "message": f"Too many environment variables (limit is {config.MAX_ENVIRON_LENGTH})",
            "code": ErrorCode.ENVIRON_TOO_LONG,
            "limit": config.MAX_ENVIRON_LENGTH
        }


class EnvironEntryTooLong(HTTPClientError):
    status_code = 413

    def json(self, /, request: web.Request, **kwargs):
        return {
            "message": f"An environment variable is too long (limit is {config.MAX_ENVIRON_LENGTH})",
            "code": ErrorCode.ENVIRON_ENTRY_TOO_LONG,
            "limit": config.MAX_ENVIRON_ENTRY_LENGTH
        }


class EnvironWithoutEquals(HTTPClientError):
    status_code = 400

    def json(self, /, request: web.Request, **kwargs):
        return {
            "message": f"An environment variable is missing the '=' sign",
            "code": ErrorCode.ENVIRON_ENTRY_MISSING_EQUALS
        }


class InvalidApiToken(HTTPClientError):
    status_code = 401

    def json(self, /, request: web.Request, **kwargs):
        pass  # TODO

    # TODO: WWW-Authenticate
