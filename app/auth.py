from aiohttp import web

from . import errors


@web.middleware
async def auth_middleware(request: web.Request, handler: web.RequestHandler):
    if request.headers.get("X-Api-Token", "") != "TOKEN":
        raise errors.
