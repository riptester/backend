from __future__ import annotations

import asyncio
import struct
from collections import AsyncIterable, AsyncGenerator
from dataclasses import dataclass
from typing import Union

from . import config
from .gatekeeper_client import write_var_string, write_var_string_array, write_int, Client
from .languages import Language
from .utils import get_runtime


async def run_code(client: Client, language: Language, code: AsyncIterable[bytes], stdin: AsyncIterable[bytes],
                   environment: list[bytes]) -> AsyncGenerator[Union[ProcessResult, ProcessOutput], bool]:
    reader, writer, session = client.start_session()
    request = (
        write_var_string(language.image.encode("latin-1"))
        + write_var_string_array([arg.encode("latin-1") for arg in language.arguments])
        + write_var_string_array(environment)
        + write_int(config.EXEC_TIME)
        + write_int(config.STOP_TIMEOUT)
        + write_int(config.TMPFS_SIZE)
        + write_int(config.MEMORY_LIMIT)
        + write_int(config.PIDS_LIMIT)
        + write_int(config.CPU_PERIOD)
        + write_int(config.CPU_QUOTA)
    )
    writer.write(struct.pack("!II", session, len(request)) + request)
    async for chunk in code:
        if not chunk:
            continue
        writer.write(struct.pack("!II", session, len(chunk) + 4) + write_var_string(chunk))
    writer.write(struct.pack("!III", session, 4, 0))
    asyncio.create_task(feed_data(stdin, writer, session))
    while packet := await reader.get():
        print(packet, packet[0])
        if packet[0] == 0xff:
            print("end")
            rc, start_length = struct.unpack_from("!hI", packet, 1)
            start = packet[7:7 + start_length].decode("latin-1")
            end = packet[7 + start_length:].decode("latin-1")
            print(start, end)
            print(start, end)
            runtime = get_runtime(start, end)
            yield ProcessResult(rc, runtime)
            return
        elif packet[0] in (1, 2):
            yield ProcessOutput(packet[0], packet[1:])


async def feed_data(stdin: AsyncIterable[bytes], writer: asyncio.StreamWriter, session: int):
    async for chunk in stdin:
        if not chunk:
            continue
        writer.write(struct.pack("!II", session, len(chunk) + 4) + write_var_string(chunk))


@dataclass(frozen=True)
class ProcessResult:
    rc: int
    runtime: int


@dataclass(frozen=True)
class ProcessOutput:
    stream: int
    data: bytes
